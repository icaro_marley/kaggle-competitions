# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 14:49:38 2018

@author: Icaro
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 13:44:57 2018

@author: Icaro
"""

'''
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys
sys.path.append('../../')
from model_tools import random_state, greedy_forward_search, grid_searchCV
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor, AdaBoostRegressor, ExtraTreesRegressor,BaggingRegressor
from sklearn.metrics import mean_squared_error
from sklearn.base import clone
from sklearn.model_selection import ParameterGrid
from sklearn.svm import SVR
from sklearn.linear_model import Lasso,LinearRegression,Ridge
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn import decomposition

data_path = "../Modified Data/"
output_path = '../Results/'

df_train = pd.read_csv(data_path+"train.csv")

target = 'SalePrice'

columns = [
 'GrLivArea',
 'OverallQual',
 'YearRemodAdd',
 'condition_sum','condition_mean','condition_lacking',
 'years_to_sold_built','years_to_sold_remod', 
 'quality_sum', 'quality_mean','quality_lacking',
 'area_per_room','total_baths',
 'LotArea',
 'OverallCond',
 'TotalBsmtSF',
 'GarageArea',

 'Neighborhood',
 'MSSubClass','LotShape',
 'BsmtExposure','BsmtFinType1','Exterior1st',
 'Exterior2nd','Foundation','GarageFinish',
 'GarageType','MasVnrType',
 'HouseStyle',
 ]

# analysis
'''
numerical_columns= [
 'YearBuilt', 
 'GrLivArea', 'OverallQual',
 'TotalBsmtSF', 
 'BsmtFinSF1', '2ndFlrSF',  
 'GarageCars',  'Fireplaces',
 'YearRemodAdd',  'LotArea',
 ]

categorical_columns = [
 'Neighborhood',
 'MSSubClass',
 'LotShape',
 'BsmtQual',
 'ExterQual',
 'FireplaceQu',
 'HeatingQC',
 'HouseStyle',
 'KitchenQual',
 'BsmtExposure',
 'BsmtFinType1',
 'Exterior1st',
 'Exterior2nd',
 'Foundation',
 'GarageFinish',
 'GarageType',
 'MasVnrType']
# descriptive

for column in numerical_columns:
    print(column)
    print("percent of nulls",df_train[column].isnull().sum()/df_train.shape[0])
    df_train[column].hist()
    plt.show()

no nulls
LotArea few different values
2ndFlrSfF few different values 


# bivariate

for column in numerical_columns:
    sns.lmplot(column,target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.13})
    plt.show()
YearBuilt  non linear pattern high spread over x, outliers on y axis
GrLivArea linear cone shaped pattern, good predictor. outliers over y and x
OverallQual linear pattern. outliers over y. spread over x
TotalBsmtSF values are very spread over y axis and not spread over x axis. Outliers on both axis
BsmtFinSF1 values at zero. outliers on both axis, low spread over x axis and high spread on y.
BsmtFinSF2 values at zero. outliers on both ais. low spread over x.
2ndFlrSF values at zero. spread y. spread x, outliers over y  > categorize above zero
GarageCars linear pattern. spread over y, few outliers over y
Fireplaces spread over y, few outliers over y
YearRemodAdd spread over x, nice auxiliar variable. non linear pattern, values at zero, outliers y

YearBuilt
GrLivArea
OverallQual
YearRemodAdd

area_per_room
total_baths
LotArea
OverallCond
years_to_sold_remod
years_to_sold_built
TotalBsmtSF
GarageArea

df_train['total_baths'] = df_train['FullBath'] + df_train['HalfBath']/2 \
 + df_train['BsmtFullBath'] + df_train['BsmtHalfBath']/2
sns.lmplot('total_baths',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':1})

df_train['area_per_room'] = df_train['GrLivArea'] / df_train['TotRmsAbvGrd']
sns.lmplot('approx_area_per_room',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':.3})

df_train['year_month'] =  df_train['YrSold'] + df_train['MoSold']/12
df_train['years_to_sold_built'] =  df_train['year_month'] - df_train['YearBuilt']
df_train['years_to_sold_remod'] =  df_train['year_month'] - df_train['YearRemodAdd']
sns.lmplot('year_month',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.5})
sns.lmplot('years_to_sold_built',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.5})
sns.lmplot('years_to_sold_remod',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.5})

#sns.lmplot('quality_sum',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.13})
# good
#sns.lmplot('quality_mean',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.13})
# good
#sns.lmplot('quality_lacking',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.13})
# might be good

df_train['m_sold_quarter'] = pd.cut(df_train['MoSold'],[0,3,6,9,12],labels=[1,2,3,4])

for column in categorical_columns:
    print(column)
    print(df_train[column].value_counts(dropna=False))
    sns.violinplot(x=column,scale='count',y=target,data=df_train,cut=0)
    plt.xticks(rotation=45)
    plt.show()
    #print('waiting')
    #input()
#Alley,Fence,  too much nulls

Neighborhood OldTown NAmes CollgCr Edwards
MSSubClass 60 20
LotShape IR1 Reg 
BsmtQual Gd TA Ex
ExterQual Gd TA
FireplaceQu Gd
HeatingQC Ex Gd TA
HouseStyle 1Story 2Story 1.5Fin
KitchenQual Gd TA Ex
BsmtExposure No Gd Av
BsmtFinType1 GLQ ALQ Unf  BLQ Rec
Exterior1st HdBoard Wd Sdng  MetalSd
Exterior2nd HdBoard  MetalSd Plywood
Foundation PConc CBlock BrkTil
GarageFinish RFn Unf Fin
GarageType Detchd
MasVnrType None Stone BrkFace






df = df_train



indicator_columns = [
 'ExterQual','BsmtQual',
 'HeatingQC','KitchenQual', 
 'FireplaceQu','GarageQual',
 'PoolQC',
 ]
indicator_values = {
   'Ex':10,
   'Gd':8,
   'TA':6,
   'Fa':4,
   'Po':2,
   'NA':0,
   np.nan:0,
}  

def sum_indicators(row):
    indicator_sum = 0 
    for column in indicator_columns:
        indicator_sum += indicator_values[row[column]]
    return indicator_sum
def count_indicators(row):
    count = 0 
    for column in indicator_columns:
        if indicator_values[row[column]] > 0:    
            count += 1
    return count    
df['quality_sum'] = df.apply(sum_indicators,axis=1)
df['quality_mean'] = df['quality_sum'] / df.apply(count_indicators,axis=1)
df['quality_lacking'] = len(indicator_columns) - df.apply(count_indicators,axis=1)


indicator_columns = [
 'ExterCond','BsmtCond',
 'HeatingQC','GarageCond','PoolQC'
 ]

indicator_values = {
   'Ex':10,
   'Gd':8,
   'TA':6,
   'Fa':4,
   'Po':2,
   'NA':0,
   np.nan:0,
}  

def sum_indicators(row):
    indicator_sum = 0 
    for column in indicator_columns:
        indicator_sum += indicator_values[row[column]]
    return indicator_sum
def count_indicators(row):
    count = 0 
    for column in indicator_columns:
        if indicator_values[row[column]] > 0:    
            count += 1
    return count    
df['condition_sum'] = df.apply(sum_indicators,axis=1)
df['condition_mean'] = df['condition_sum'] / df.apply(count_indicators,axis=1)
df['condition_lacking'] = len(indicator_columns) - df.apply(count_indicators,axis=1)

df['rating'] = df['OverallQual'] + df['OverallCond'] + df['condition_sum'] + df['quality_sum']
df['lacking'] = (df['quality_lacking'] + df['condition_lacking'])

sns.lmplot('teste',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.13})
----------------

column ='Neighborhood'
print(df[column].value_counts(dropna=False))
sns.violinplot(x=column,scale='count',y=target,data=df_train,cut=0)


'Neighborhood',
 
 
'MSSubClass',


'Exterior1st',
 


to_replace = {
   'Reg':10,
   'IR1':'Ireg',
   'IR2':'Ireg',
   'IR3':'Ireg',
}   
df['lot_reg'] = df['LotShape'].replace(to_replace) 


to_replace = {
   'Fin':10,
   'RFn':8,
   'Unf':4,
   'NA':0,
   np.nan:0,
}   
df['garage_finish'] = df['GarageFinish'].replace(to_replace) 



 ADICIONAR MAIS EM QUALIDADE
 
indicator_values = {
   'GLQ':10,
   'ALQ':8,
   'BLQ':7,
   'Rec':6,
   'LwQ':4,
   'Unf':2,
   'NA':0,
   np.nan:0,
}   
df['qual_basement'] = (df['BsmtFinType1'].replace(indicator_values) + df['BsmtFinType2'].replace(indicator_values))/2
sns.lmplot('qual_basement',target,data=df_train,fit_reg=False,scatter_kws={'s':3,'alpha':0.13})

'''

#df_train[target].hist(bins=50)
#df = df_train[df_train[target] >= 106475]
#df[target].hist(bins=50)
#pd.qcut(df_train[target],10)

def score_func(y_true,y_pred):
    #return np.sqrt(mean_squared_error(y_true, y_pred))
    return np.sqrt(mean_squared_error(np.log(y_true), np.log(y_pred)))

def select_x_y(df,columns,train_values=None,return_y=True):
    columns_aux = columns
    
    #outlier removal
    #if train_values is None:
    #    df = df[df[target] >= 106475].copy() # 106475
        
    # preprocessing   
    if 'MSSubClass' in columns:
        to_replace = {
        20:'1story',30:'1story',40:'1story',120:'1story',
        60:'2story',70:'2story',160:'2story',
        75:'1/2story',45:'1/2story',50:'1/2story',150:'1/2story',
        80:'split',85:'split',
        90:'duplex',
        180:'pud',
        190:'family',
        }
        df['MSSubClass'] = df['MSSubClass'].replace(to_replace)
    if 'HouseStyle' in columns:
        to_replace = {
        '1.5Fin':'1Story','1.5Unf':'1Story',
        '2.5Fin':'2Story','2.5Unf':'2Story',
        }
        df['HouseStyle'] = df['HouseStyle'].replace(to_replace)

    # feature engineering
    if 'm_sold_quarter' in columns:
        df['m_sold_quarter'] = pd.cut(df['MoSold'],[0,3,6,9,12],
          labels=[1,2,3,4]).astype(int)
    if 'area_per_room' in columns_aux:
        df['area_per_room'] = df['GrLivArea'] / df['TotRmsAbvGrd']
    if 'total_baths' in columns_aux:
        df['total_baths'] = df['FullBath'] + df['HalfBath']/2 \
            + df['BsmtFullBath'] + df['BsmtHalfBath']/2
    if len(set(['years_to_sold_built','years_to_sold_remod']).\
           intersection(set(columns_aux))) > 0:
        df['year_month'] =  df['YrSold'] + df['MoSold']/12
        df['years_to_sold_built'] =  df['year_month'] - df['YearBuilt']
        df['years_to_sold_remod'] =  df['year_month'] - df['YearRemodAdd']
    if len(set(['quality_sum','quality_mean','quality_lacking']).\
           intersection(set(columns_aux))) > 0:
        indicator_columns = [
         'ExterQual','BsmtQual',
         'HeatingQC','KitchenQual', 
         'FireplaceQu','GarageQual',
         'PoolQC',
         ]
        indicator_values = {
           'Ex':10,
           'Gd':8,
           'TA':6,
           'Fa':4,
           'Po':2,
           'NA':0,
           np.nan:0,
        }  
        
        def sum_indicators(row):
            indicator_sum = 0 
            for column in indicator_columns:
                indicator_sum += indicator_values[row[column]]
            return indicator_sum
        def count_indicators(row):
            count = 0 
            for column in indicator_columns:
                if indicator_values[row[column]] > 0:    
                    count += 1
            return count    
        df['quality_sum'] = df.apply(sum_indicators,axis=1)
        df['quality_mean'] = df['quality_sum'] / df.apply(count_indicators,axis=1)
        df['quality_lacking'] = len(indicator_columns) - df.apply(count_indicators,axis=1)
    
    if len(set(['condition_sum','condition_mean','condition_lacking']).\
               intersection(set(columns_aux))) > 0:
        indicator_columns = [
         'ExterCond','BsmtCond',
         'HeatingQC','GarageCond','PoolQC'
         ]
    
        indicator_values = {
           'Ex':10,
           'Gd':8,
           'TA':6,
           'Fa':4,
           'Po':2,
           'NA':0,
           np.nan:0,
        }  
        
        def sum_indicators(row):
            indicator_sum = 0 
            for column in indicator_columns:
                indicator_sum += indicator_values[row[column]]
            return indicator_sum
        def count_indicators(row):
            count = 0 
            for column in indicator_columns:
                if indicator_values[row[column]] > 0:    
                    count += 1
            return count    
        df['condition_sum'] = df.apply(sum_indicators,axis=1)
        df['condition_mean'] = df['condition_sum'] / df.apply(count_indicators,axis=1)
        df['condition_lacking'] = len(indicator_columns) - df.apply(count_indicators,axis=1)        
    # selecting values
    dict_values ={
     'BsmtExposure': ['No', 'Gd', 'Av'],
     'BsmtFinType1': ['GLQ', 'ALQ', 'Unf', 'BLQ', 'Rec'],
     'Exterior1st': ['HdBoard', 'Wd Sdng', 'MetalSd'],
     'Exterior2nd': ['HdBoard', 'MetalSd', 'Plywood'],
     'FireplaceQu': ['Gd'],
     'Foundation': ['PConc', 'CBlock', 'BrkTil'],
     'GarageFinish': ['RFn', 'Unf', 'Fin'],
     'GarageType': ['Detchd'],
     'HouseStyle': ['1Story', '2Story', '1.5Fin'],
     'LotShape': ['IR1', 'Reg'],
     'MasVnrType': ['None', 'Stone', 'BrkFace'],
     'Neighborhood': ['OldTown', 'NAmes', 'CollgCr', 'Edwards']
    }
    for column, values in dict_values.items():
        if column in columns_aux:
            df[column] = df[column].apply(
                lambda x:x if x in values else 'other')
    
    # fill with mode and mean
    # dummify
    # scale
    x = df[columns_aux].copy()
    if train_values is None:
        train_values = {}
        for column in columns_aux:
            numerical = False
            if np.issubdtype(x[column].dtype, np.number):
                  values = [x[column].mean()] # mean as most common value
                  numerical = True
            else: # categorical
                values = x[column].value_counts().index.values

            #fill nan with most frequent
            x[column] = x[column].fillna(values[0])
            if not numerical:
                # dummification
                column_dummies = pd.get_dummies(x[column],prefix=column,prefix_sep='_')
                x = pd.concat([x, column_dummies], axis=1)
                # other column
                if column+'_other' not in x.columns:
                    x[column+'_other'] = 0
                    values = np.append(values,'other')
                del x[column]
            train_values[column] = values
        # train scaler
        scaler = StandardScaler().fit(x)
        train_values['__scaler'] = scaler
    else:
        for column, values in train_values.items():
            if column == '__scaler': # scaler object
                continue
            x[column] = x[column].fillna(values[0]) # fill nan with most frequent
            # select most frequent
            if not np.issubdtype(df[column].dtype, np.number):
                x[column] = x[column].apply(lambda x: x if x in values else 'other')
                all_values = x[column].unique()
                # add columns of non existent frequent values
                for value in values:
                    if value not in all_values:
                        x[column+'_'+str(value)] = 0
                # dummification
                column_dummies = pd.get_dummies(x[column],prefix=column,prefix_sep='_')
                x = pd.concat([x, column_dummies], axis=1)
                del x[column]
    
    # scale
    x = train_values['__scaler'].transform(x)
    if return_y:
        y = df[target]
        return x,y,train_values
    return x,train_values
'''
model = RandomForestRegressor(n_estimators = 100, random_state=random_state)
score_list,score_mean, score_std, score_list = k_fold(df_train,select_x_y,model,score_func,10)
'''

model_classes = []
parameter_list = []

'''
model_classes.append(GradientBoostingRegressor)
parameter_list.append({
        'n_estimators':[100],
        'max_depth':np.arange(3,5),
        'random_state':[random_state],
        })    
model_classes.append(BaggingRegressor)
parameter_list.append({
        'base_estimator':[DecisionTreeRegressor(random_state=random_state,max_depth=None)],
        'n_estimators':[100],
        'random_state':[random_state],
        'bootstrap_features':[True],
        })  
model_classes.append(AdaBoostRegressor)
parameter_list.append({
        'base_estimator':[DecisionTreeRegressor(random_state=random_state,max_depth=None)],
        'n_estimators':[100],
        'random_state':[random_state],
        })  

model_classes.append(ExtraTreesRegressor)
parameter_list.append({
        'n_estimators':[100],
        'random_state':[random_state],
        'n_jobs':[-1],
        })        

model_classes.append(RandomForestRegressor)
parameter_list.append({
        'n_estimators':[100],
        'random_state':[random_state],
        'n_jobs':[-1],
        }) 

model_classes.append(MLPRegressor)
parameter_list.append({
        'hidden_layer_sizes':[(100,100,100,100,100)],
        'learning_rate':['adaptive'],
        'random_state':[random_state],
        'max_iter':[200],
        })    
'''

'''
model_classes.append(DecisionTreeRegressor)
parameter_list.append({
        'random_state':[random_state],
        })    

model_classes.append(KNeighborsRegressor)
parameter_list.append({
        'weights':['distance'],
        'n_neighbors':np.arange(5,7),
        'n_jobs':[-1],
        'p':[1],
        })    
        
model_classes.append(SVR)
parameter_list.append({
        'C':[1],
        'kernel':['linear'],
        })

model_classes.append(LinearRegression)
parameter_list.append({
        })   
'''

model_classes.append(GradientBoostingRegressor)
parameter_list.append({
        #'loss':['ls','lad','huber','quantile'],
        'n_estimators':[50,100],
        'max_depth':np.arange(3,6),
        'random_state':[random_state],
        })    
     
# creates a list of models based on its parameters
model_list = []
for model,parameters in zip(model_classes,parameter_list):
    param_grid = list(ParameterGrid(parameters))
    for params in param_grid:
        model_list.append(model(**params))

args = {
    'data':df_train,
    'columns':columns,
    'select_func':select_x_y,
    'score_func':score_func,
    'model_list':model_list,
    'k':10,
}
df = greedy_forward_search(args,opt='lower')


'''

columns = [
        'OverallQual', 
        'GrLivArea',
        'years_to_sold_built',
        'OverallCond', 
        'TotalBsmtSF',
        'LotArea', 
        'quality_lacking',
        'Neighborhood', 
        'total_baths', 
        'BsmtExposure',
        'GarageArea',
        
         'condition_lacking',
         'quality_sum', 

        'MasVnrType',
        'BsmtFinType1', 
        'Exterior2nd',
        'Foundation',
        'GarageFinish',
        'GarageType',
        'HouseStyle',
        ]
        


columns = [
'Neighborhood',
'MSSubClass',
'LotShape',
'BsmtExposure',
'BsmtFinType1',
'Exterior1st',
'Exterior2nd',
'Foundation',
'GarageFinish',
'GarageType',
#'MasVnrType',
'HouseStyle',
]
select_func = lambda x,y=None:select_x_y(x,columns=columns,train_values=y)
df = grid_searchCV(df_train,select_func,model_list,score_func,10)
df['mean test score'].min()
'''

#df.to_csv('best_results.csv',index=False)
#'''
# submission
'''
# train final model 
model_final = clone(model)
X_train, y_train, train_values = select_x_y(df_train,columns)
model_final.fit(X_train,y_train)

#plt.scatter(model_final.predict(X_train),y_train,s=1)


# test ouput
df_test = pd.read_csv(data_path+'test.csv')
X_test, train_values = select_x_y(df_test, columns,train_values,return_y=False)
output = pd.DataFrame([])
output['Id'] = df_test.Id
output['SalePrice'] = model_final.predict(X_test)
output.to_csv(output_path+'output.csv',index=False)




'''


'''
['OverallQual', 'GrLivArea', 'TotalBsmtSF', 'years_to_sold_built', 'quality_sum', 'LotArea']
KNeighborsRegressor  n_neighbors=6
30457.8


columns = ['OverallQual', 'GrLivArea', 'years_to_sold_built', 'TotalBsmtSF', 'OverallCond', 'total_baths', 'Neighborhood', 'LotArea', 'BsmtExposure', 'MasVnrType']
model = GradientBoostingRegressor(alpha=0.9, criterion='friedman_mse', init=None,
             learning_rate=0.1, loss='ls', max_depth=4, max_features=None,
             max_leaf_nodes=None, min_impurity_decrease=0.0,
             min_impurity_split=None, min_samples_leaf=1,
             min_samples_split=2, min_weight_fraction_leaf=0.0,
             n_estimators=150, presort='auto', random_state=100,
             subsample=1.0, verbose=0, warm_start=False)
0.14160

columns = ['OverallQual', 'GrLivArea', 'years_to_sold_built', 'OverallCond', 'TotalBsmtSF', 'LotArea', 'quality_lacking', 'Neighborhood', 'total_baths', 'BsmtExposure', 'GarageArea']
model = GradientBoostingRegressor(alpha=0.9, criterion='friedman_mse', init=None,
             learning_rate=0.1, loss='ls', max_depth=4, max_features=None,
             max_leaf_nodes=None, min_impurity_decrease=0.0,
             min_impurity_split=None, min_samples_leaf=1,
             min_samples_split=2, min_weight_fraction_leaf=0.0,
             n_estimators=100, presort='auto', random_state=100,
             subsample=1.0, verbose=0, warm_start=False)
0.14658
'''