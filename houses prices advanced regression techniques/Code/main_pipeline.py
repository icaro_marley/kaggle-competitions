# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 14:49:38 2018

@author: Icaro
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys
sys.path.append('../../')
from model_tools import random_state,FeatureSelectorTrainer,OutlierRemoval, ModelTransformer, IdentityTransformer, TransformModeler, AbsoluteProcessor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor, AdaBoostRegressor, ExtraTreesRegressor,BaggingRegressor
from sklearn.metrics import mean_squared_error
from sklearn.base import clone
from sklearn.model_selection import ParameterGrid,GridSearchCV
from sklearn.svm import SVR,LinearSVR,NuSVR
from sklearn.linear_model import Lasso,LinearRegression,Ridge
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn import decomposition
from xgboost import XGBRegressor
from sklearn.pipeline import Pipeline,FeatureUnion
from sklearn.preprocessing import Imputer,RobustScaler, MaxAbsScaler, MinMaxScaler
from sklearn.feature_selection import VarianceThreshold,SelectFromModel,RFE
from sklearn.decomposition import PCA,KernelPCA,SparsePCA
from mlxtend.regressor import StackingCVRegressor


data_path = "../Modified Data/"
output_path = '../Results/'


'''
variables

>house
type of house BldgType
quality OverallQual
condition  OverallCond
n floors HouseStyle (to numerical) https://www.boards.ie/b/thread/2054925042
year build YearBuilt

>stuff inside
year modifications YearRemodAdd
basement area TotalBsmtSF 
living area above ground GrLivArea
total vip living area  WoodDeckSF PoolArea OpenPorchSF EnclosedPorch 3SsnPorch ScreenPorch(sum)
garage size  GarageArea
home funcionality Functional (numerical)


>terrain
lot LotConfig
shape LotShape LandContour  LandSlope (numerical, each)
area LotArea

>surroundings
area MSZoning
neighborhood Neighborhood C
neighborhood2 ondition1 Condition2 
access area LotFrontage
Utilities Utilities (numerical)

>sale
time YrSold MoSold
type SaleType
condition SaleCondition
'''

# pre processing
# join dfs
df_train = pd.read_csv(data_path+"train.csv")
df_train['type'] = 'train'
df_test = pd.read_csv(data_path+'test.csv')
df_test['type'] = 'test'
df_all = pd.concat([df_train,df_test])
data_origin =  df_all['type']
# pre analysis
#percent of nulls 
#(df_all.isnull().sum()/df_all.shape[0]).sort_values()
# diversity
#df_all.apply(lambda x:x.unique().shape[0]/x.shape[0]).sort_values()

# numerical
to_numerical_dict = {
       '1Story':1,'1.5Fin':1.5,'1.5Unf':1.5,'2Story':2,
       '2.5Fin':2.5,'2.5Unf':2.5,'SFoyer':2,'SLvl':2,
}
df_all['HouseStyle'] = df_all['HouseStyle'].replace(to_numerical_dict)

to_numerical_dict = {      
       'Typ':10,'Min1':8,'Min2':6.5,'Mod':5,
       'Maj1':4,'Maj2':2,'Sev':1,'Sal':0,
}
df_all['Functional'] = df_all['Functional'].replace(to_numerical_dict)

to_numerical_dict = {      
        'Reg':10,'IR1':6.6,'IR2':3.3,'IR3':0,  
}
df_all['LotShape'] = df_all['LotShape'].replace(to_numerical_dict)

to_numerical_dict = {      
       'Lvl':0,'Bnk':1,'HLS':2,'Low':-1,
}
df_all['LandContour'] = df_all['LandContour'].replace(to_numerical_dict)

to_numerical_dict = {      
       'Gtl':0,'Mod':5,'Sev':10, 
}
df_all['LandSlope'] = df_all['LandSlope'].replace(to_numerical_dict)

to_numerical_dict = {      
       'AllPub':3,'NoSewr':2,'NoSeWa':1,'ELO':0,    
}
df_all['Utilities'] = df_all['Utilities'].replace(to_numerical_dict)

'''
for column in ['HouseStyle','Functional','LotShape',
    'LandContour','LandSlope','Utilities']:
    print(df_all[column].dtype)
'''
# map similar categories
map_dict = {
        'Feedr':'street','Artery':'street','RRNn':'railroad',
        'RRAn':'railroad','RRNe':'railroad','RRAe':'railroad',
        'PosN':'positive off', 'PosA':'positive off',
}
df_all['Condition1'] = df_all['Condition1'].replace(map_dict)
df_all['Condition2'] = df_all['Condition2'].replace(map_dict)
       
df_all['total_vip_area'] = df_all['WoodDeckSF'] + df_all['PoolArea']\
     + df_all['OpenPorchSF'] + df_all['EnclosedPorch'] + df_all['3SsnPorch']\
     + df_all['ScreenPorch'] 

# selection
columns = [
    'BldgType','OverallQual','OverallCond','HouseStyle','YearBuilt',  
    'YearRemodAdd','TotalBsmtSF','GrLivArea','total_vip_area','GarageArea',
    'Functional','LotConfig','LotShape','LandContour','LandSlope','LotArea',
    'MSZoning','Neighborhood','Condition1','Condition2','LotFrontage','Utilities',
    'YrSold','MoSold','SaleType','SaleCondition','SalePrice'
]
df_all = df_all[columns]

# pd.dummies
df_all = pd.get_dummies(df_all)

# split
df_all['type'] = data_origin
df_train = df_all[df_all['type'] == 'train']
df_test = df_all[df_all['type'] == 'test']
del df_train['type'],df_test['type']

columns = df_train.columns
# pipeline

def score_func(estimator, X_test, y_true):
    y_pred = estimator.predict(X_test)
    return np.sqrt(mean_squared_error(np.log(y_true), np.log(y_pred)))

target = 'SalePrice' 
columns = np.delete(columns,np.where(columns==target))

'''
fill na -> scale -> model

RobustScaler
MaxAbsScaler
MinMaxScaler
StandardScaler

VarianceThreshold
SelectFromModel
'''

'''
pip = Pipeline([
        #('remover_target_outliers',UpperOutlierRemover(20)),
        ("imputer", Imputer(missing_values=np.nan,
            strategy='most_frequent',
            axis=0)),
        ('scaler',RobustScaler()),
        ('feature_selection1',VarianceThreshold()),
        ('feature_selection2',SelectFromModel(DecisionTreeRegressor(
                random_state=random_state))),
        #('id',IdentityTransformer()),
        #('pca',KernelPCA(8)),
        ("regressor", ModelTransformer(
            GradientBoostingRegressor(random_state=random_state))),
        #('f_selector_model',FeatureSelectorTrainer(
        #    SelectFromModel(GradientBoostingRegressor(random_state=random_state)))),
        ('abs',TransformModeler(AbsoluteProcessor())),
]) 

estimators = [
    ###RandomForestRegressor(random_state=random_state),
    ###GradientBoostingRegressor(random_state=random_state),
    #AdaBoostRegressor(random_state=random_state),
    #ExtraTreesRegressor(random_state=random_state),
    #BaggingRegressor(random_state=random_state),
    ###SVR(kernel='linear'),
    ###KNeighborsRegressor(),
    #MLPRegressor((100,100,100,100,100,100,100),random_state=random_state),
    ###LinearRegression()
]


selectors = [
    #SelectFromModel(LinearRegression()),   
    #SelectFromModel(RandomForestRegressor(random_state=random_state)),
    #SelectFromModel(GradientBoostingRegressor(random_state=random_state)),
    #SelectFromModel(AdaBoostRegressor(random_state=random_state)),
    #SelectFromModel(ExtraTreesRegressor(random_state=random_state)),
    IdentityTransformer(),
]

scalers = [
    RobustScaler(),
    MaxAbsScaler(),
    MinMaxScaler(),
    StandardScaler(),
]

# cross validation
param_grid = [
    {
        #'scaler': scalers,
        'feature_selection1__threshold':[0],#np.arange(0,0.11,0.02),
        'feature_selection2': selectors,
        #'regressor__model__C':np.arange(1,200,50),
        #'regressor__model__epsilon':[1.9],
        #'regressor__model':estimators,
        'regressor__block__model__n_estimators':np.arange(50,101,1),
        #'regressor__model__max_depth':[None],
        #'regressor__model__min_samples_split':np.arange(2,101,10),
        #'regressor__model__min_samples_leaf':np.arange(1,101,10),
        #'regressor__model__weights':['distance'],
        #'regressor__model__n_neighbors':[10],
        #'regressor__model__p':[1,2],
    },
]


grid = GridSearchCV(pip, cv=10, n_jobs=1, param_grid=param_grid,
    scoring=score_func,verbose=1)
grid.fit(df_train[columns].values,df_train[target].values)



mean_scores = np.array(grid.cv_results_['mean_test_score'])
std_scores = np.array(grid.cv_results_['std_test_score'])
df_results = pd.DataFrame([mean_scores,std_scores]).T
df_results.columns = ['mean','std']
params = [str(params) for params in grid.cv_results_['params']]
df_results['params'] = params
'''



'''
LinearRegression() {'feature_selection1__threshold': 0.0, 'feature_selection2': IdentityTransformer(), 'scaler': MaxAbsScaler(copy=True)}
0.184108 0.0319175
('abs',TransformModeler(AbsoluteProcessor())),


{'feature_selection1__threshold': 0.1, 
'feature_selection2': SelectFromModel(estimator=GradientBoostingRegressor(alpha=0.9, criterion='friedman_mse', init=None,
             learning_rate=0.1, loss='ls', max_depth=3, max_features=None,
             max_leaf_nodes=None, min_impurity_decrease=0.0,
             min_impurity_split=None, min_samples_leaf=1,
             min_samples_split=2, min_weight_fraction_leaf=0.0,
             n_estimators=100, presort='auto', random_state=100,
             subsample=1.0, verbose=0, warm_start=False),
        norm_order=1, prefit=False, threshold=None),
 'regressor__model__n_neighbors': 10, 
 'regressor__model__weights': 'distance', 
 'scaler': RobustScaler(copy=True, quantile_range=(25.0, 75.0), with_centering=True,
       with_scaling=True)}
0.160756 0.0184919
        ("regressor", ModelTransformer(
            KNeighborsRegressor(n_jobs=-1))),
        
no scaler   
no feature selector
GradientBoostingRegressor(random_state=random_state)
0.136788 0.0202087

SVR(kernel='linear')
{'feature_selection1__threshold': 0, 'feature_selection2': IdentityTransformer(), 'regressor__model__C': 101, 'regressor__model__epsilon': 1.9, 'scaler': StandardScaler(copy=True, with_mean=True, with_std=True)}
0.149011 0.0196977

RandomForestRegressor(random_state=random_state)
{'regressor__model__max_depth': None, 'regressor__model__n_estimators': 150}
0.149482 0.0180152 {'feature_selection1__threshold': 0.02, 'feature_selection2': IdentityTransformer(), 'regressor__model__max_depth': None, 'regressor__model__n_estimators': 150}

ExtraTreesRegressor(random_state=random_state)
{'feature_selection1__threshold': 0.04, 'feature_selection2': IdentityTransformer(), 'regressor__model__min_samples_leaf': 1, 'regressor__model__min_samples_split': 2, 'regressor__model__n_estimators': 100}
0.14267993051847122 0.01689242323924518

'''

# stacking
base_models = []

base_models.append(Pipeline([
        ("imputer", Imputer(missing_values=np.nan,
            strategy='most_frequent',
            axis=0)),
        ('scaler',MaxAbsScaler()),
        ('feature_selection1',VarianceThreshold()),
        ("regressor", ModelTransformer(
            LinearRegression())),
        ('abs',TransformModeler(AbsoluteProcessor())),
]))

base_models.append(Pipeline([
        ("imputer", Imputer(missing_values=np.nan,
            strategy='most_frequent',
            axis=0)),
        ('scaler',RobustScaler()),
        ('feature_selection1',VarianceThreshold(0.1)),
        ('feature_selection2',SelectFromModel(GradientBoostingRegressor(
                random_state=random_state))),
        ("regressor", ModelTransformer(
            KNeighborsRegressor(n_neighbors=10,weights='distance'))),
        ('abs',TransformModeler(AbsoluteProcessor())),
]))
          
base_models.append(Pipeline([
        ("imputer", Imputer(missing_values=np.nan,
            strategy='most_frequent',
            axis=0)),
        ("regressor", ModelTransformer(
            GradientBoostingRegressor(random_state=random_state))),
        ('abs',TransformModeler(AbsoluteProcessor())),
]))
        
base_models.append(Pipeline([
        ("imputer", Imputer(missing_values=np.nan,
            strategy='most_frequent',
            axis=0)),
        ('scaler',StandardScaler()),
        ('feature_selection1',VarianceThreshold()),
        ("regressor", ModelTransformer(
            SVR(kernel='linear',C=101,epsilon=1.9))),
        ('abs',TransformModeler(AbsoluteProcessor())),
]))

base_models.append(Pipeline([
        ("imputer", Imputer(missing_values=np.nan,
            strategy='most_frequent',
            axis=0)),
        ('feature_selection1',VarianceThreshold(0.02)),
        ("regressor", ModelTransformer(
            RandomForestRegressor(random_state=random_state,n_estimators=150))),
        ('abs',TransformModeler(AbsoluteProcessor())),
]))
 
base_models.append(Pipeline([
        ("imputer", Imputer(missing_values=np.nan,
            strategy='most_frequent',
            axis=0)),
        ('feature_selection1',VarianceThreshold(0.04)),
        ("regressor", ModelTransformer(
            ExtraTreesRegressor(random_state=random_state,n_estimators=100))),
        ('abs',TransformModeler(AbsoluteProcessor())),
]))       

regressors = base_models
meta_regressor = XGBRegressor(n_jobs=-1,random_state=random_state,max_depth=4,n_estimators=50)
stck_model = StackingCVRegressor(
    regressors=regressors,meta_regressor=meta_regressor,cv=10)

#stck_model.fit(df_train[columns].values,df_train[target].values)
#p = stck_model.predict(df_train[columns])

stck_pipeline = Pipeline([
        ("regressor", ModelTransformer(
            stck_model)),
        ('abs',TransformModeler(AbsoluteProcessor())),
])    
'''
param_grid = [{
    'regressor__model__meta-xgbregressor__max_depth':[4],
    'regressor__model__meta-xgbregressor__n_estimators':[50],
}]

grid = GridSearchCV(stck_pipeline, cv=10, n_jobs=1, param_grid=param_grid,
    scoring=score_func,verbose=2)
grid.fit(df_train[columns].values,df_train[target].values)

mean_scores = np.array(grid.cv_results_['mean_test_score'])
std_scores = np.array(grid.cv_results_['std_test_score'])
df_results = pd.DataFrame([mean_scores,std_scores]).T
df_results.columns = ['mean','std']
params = [str(params) for params in grid.cv_results_['params']]
df_results['params'] = params
'''

'''
XGBRegressor(n_jobs=-1,random_state=random_state)
#0.12938657912057494 0.021057440792651257


{'regressor__model__meta-xgbregressor__max_depth': 4, 
'regressor__model__meta-xgbregressor__n_estimators': 50}
0.1282877678708225 0.020647971182895807




{'meta-xgbregressor__max_depth': 4, 
'meta-xgbregressor__n_estimators': 50}
0.1270185247871358 0.008654785058455385

'''



# submission
'''
model = stck_pipeline
# train final model 
model_final = clone(model)
model_final.fit(df_train[columns].values,df_train[target].values)

# test ouput
output = pd.DataFrame([])
output['Id'] = pd.read_csv(data_path+'test.csv').Id
output['SalePrice'] = model_final.predict(df_test[columns])
output.to_csv(output_path+'output.csv',index=False)
'''