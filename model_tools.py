# -*- coding: utf-8 -*-
from sklearn.base import clone
from sklearn.model_selection import KFold
import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

random_state = 100
'''
# greedy forward search
# search for improvements on mean test score
def greedy_forward_search(args, opt='lower'):
    improvement = True
    best_subset = []
    best_mean_score = np.inf
    if opt == 'higher':
        best_mean_score = -np.inf
    # while there is improvement
    input_list = args['columns']
    base_func = args['select_x_func']
    df_scores = pd.DataFrame()
    while improvement:
        # search variable space
        best_mean_score_aux = best_mean_score
        for index, variable in enumerate(input_list):
            if variable  in best_subset: 
                continue
            aux_subset = best_subset + [variable]
            print("Checking:",variable)
            args['select_x_func'] = lambda x,y=None: base_func(df=x,columns=aux_subset,train_values=y)
            df_scores_aux = grid_searchCV(**args)
            # check best scores
            best_mean_score_aux2 = df_scores_aux['mean test score'].min()          
            comparison = best_mean_score_aux2 <= best_mean_score_aux 
            if opt == 'higher':
                best_mean_score_aux2 = df_scores_aux['mean test score'].max()
                comparison = best_mean_score_aux2 > best_mean_score_aux
            if comparison:
                best_mean_score_aux = best_mean_score_aux2
                pos = index
                print("LOCAL SCORE:", best_mean_score_aux)
            # keep info about testing
            df_scores_aux['columns'] = str(aux_subset)
            df_scores = df_scores.append(df_scores_aux,ignore_index=True)   
        # check best scores
        comparison = best_mean_score_aux < best_mean_score
        if opt == 'higher':
            comparison = best_mean_score_aux > best_mean_score

        if comparison:
            best_subset.append(input_list[pos])
            best_mean_score = best_mean_score_aux
            print("GLOBAL SCORE:",best_mean_score)            
            print("BEST SUBSET:",best_subset)
        else: # no improvement
            improvement = False

    return df_scores

# stacking
class StackingCVRegressor():
    
    def __init__(self,meta_model=None,base_model_list=[],select_x_func_list=[],cv=10):
        self._cv = cv
        self._meta_model = meta_model
        self._models = []
        self._errors = []
        for model,select_x_func in zip(base_model_list,
                select_x_func_list):
            self._models.append({
            'model':model,
            'select_x_func':select_x_func})

    # get meta features from fitted base models
    def _get_meta_features(self,X):               
        test_predictions = pd.DataFrame([])
        for model_info in self._models:
            model = model_info['model']
            select_x_func = model_info['select_x_func']
            train_values = model_info['train_values']
            X_test = select_x_func(X,train_values)
            test_predictions["base: "+str(model)] = model.predict(X_test)
        return test_predictions
    
    def fit(self,X,y):
        self._errors = pd.DataFrame([])
        # split data
        X_splits = k_train_test_split(X,self._cv,random_state=random_state)
        y_splits = k_train_test_split(y,self._cv,random_state=random_state)
        
        splits = []
        for X_split, y_split in zip(X_splits,y_splits):
            splits.append([X_split[0],y_split[0],X_split[1]])            
        
        cv_meta_features = pd.DataFrame([])
        for X_train_raw,y_train_raw, X_test_raw in splits:
            # fit all base models 
            for model_info in self._models:
                model = model_info['model']
                select_x_func = model_info['select_x_func']
                X_train, train_values = select_x_func(X_train_raw)
                y_train = y_train_raw
                # train, att train_values
                model_info['train_values'] = train_values
                model.fit(X_train,y_train)

            cv_meta_features = pd.concat(
                    [cv_meta_features,
                     self._get_meta_features(X_test_raw)],axis=0)
        
        errors = cv_meta_features.copy()
        y_true = y
        for column in errors.columns:
            errors[column] = errors[column].values - y_true.values
        self._errors = errors

        
        # fit meta model
        self._meta_model = clone(self._meta_model)
        X_train = cv_meta_features
        y_train = y
        self._meta_model.fit(X_train,y_train)
        # train all base models on all available data
        for model_info in self._models:
            model = model_info['model']
            select_x_func = model_info['select_x_func']
            X_train, train_values = select_x_func(X)
            
            model.fit(X_train,y_train)
            model_info['train_values'] = train_values
    
    def predict(self,X, get_base_preds=False):
        base_predictions = self._get_meta_features(X)
        preds = self._meta_model.predict(base_predictions)
        
        if get_base_preds:
            base_predictions["meta: "+str(self._meta_model)] = preds
            return base_predictions
        return preds
    
    def clone(self):
        params = {}
        params['meta_model'] = clone(self._meta_model)
        params['cv'] = self._cv
        params['base_model_list'] = []
        params['select_x_func_list'] = [] 
        for model_dict in self._models:
            params['base_model_list'].append(clone_model(model_dict['model']))
            params['select_x_func_list'].append(model_dict['select_x_func'])
        return self.__class__(**params)
    
    
    def get_models(self):
        model_list = []
        for model_info in self._models:
            model_list.append(model_info['model'])
        return model_list
    
    def __str__(self):
        return 'ST: ' + 'meta.'.str(self._meta_learner) +\
        'base.[' + \
            [str(model) for model in self.get_models()].join(" ") +\
                ']'  
'''    
# positive values transformation
class AbsoluteProcessor(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def fit(self,*args):
        return self
    
    def transform(self,values,**args):
        return np.absolute(values)
    
class ModelTransformer(TransformerMixin,BaseEstimator):

    def __init__(self, model,):
        self.model = model

    def fit(self, *args, **kwargs):
        self.model.fit(*args, **kwargs)
        return self

    def transform(self, X, **transform_params):
        return self.predict(X)

    def predict(self,X,**kwargs):
        return self.model.predict(X)


class TransformModeler(TransformerMixin,BaseEstimator):

    def __init__(self, transformer):
        self.transformer = transformer

    def fit(self, *args, **kwargs):
        self.transformer.fit(*args)
        return self

    def transform(self, X, **transform_params):
        return self.transformer.transform(X)

    def predict(self,X,**kwargs):
        return self.transform(X)

class IdentityTransformer(TransformerMixin,BaseEstimator):

    def __init__(self):
        pass

    def fit(self, *args, **kwargs):
        return self

    def transform(self, X):
        #print(X.shape)
        return X

class FeatureSelectorTrainer(TransformerMixin,BaseEstimator):

    def __init__(self, transformer):
        self.transformer = transformer
        self.model = clone(transformer.estimator)
        
    def fit(self, X, y, *args, **kwargs):
        self.transformer.fit(X,y)
        X_ = self.transformer.transform(X)
        self.model.fit(X_,y)
        return self

    def transform(self, X, **transform_params):
        return self.predict(X)

    def predict(self,X,**kwargs):
        X_ = self.transformer.transform(X)
        return self.model.predict(X_)
    
 
class OutlierRemoval(TransformerMixin, BaseEstimator):
    def __init__(self, block,q=20):
        self.q = q
        self.block = block

    def fit(self, X, y):
        _,q = pd.qcut(y,q=self.q,retbins=True)
        x_size = X.shape[1]
        x_y = np.append(X,y.reshape(-1,1),axis=1)
        x_y = x_y[np.where(y<=q[-2])]
        X_ = x_y[:,:x_size]
        y_ = x_y[:,-1]
        self.block.fit(X_,y_)
        
        return self

    def transform(self, X):
        return self.block.transform(X)