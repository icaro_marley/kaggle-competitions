# Kaggle Competitions

Repositório com scripts comuns que foram utilizados em duas competições do Kaggle:
- https://www.kaggle.com/c/digit-recognizer
- https://www.kaggle.com/c/house-prices-advanced-regression-techniques

# Tecnologias utilizadas
- Pandas e Numpy (Manipulação de dados)
- Matplotlib e Seaborn (Visualização de dados)
- Sklearn e XGBoost (Machine Learning)