# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense, Dropout,Flatten, Conv2D, MaxPool2D
from sklearn.model_selection import train_test_split
from keras import regularizers
from keras.callbacks import ModelCheckpoint,EarlyStopping

random_state = 100

data_path = '../Modified Data/'
results_path = '../Results/'
df_train = pd.read_csv(data_path+'train.csv')
df_test = pd.read_csv(data_path+'test.csv')

#df_train.isnull().any().any()
target = 'label'
columns = [column for column in df_train.columns if column != target]
# train valid split
X_train, X_val, y_train, y_val = train_test_split(df_train[columns],
    df_train[target], test_size=0.33, random_state=random_state)
X_test = df_test.values

# scaler
scaler = StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_val = scaler.transform(X_val)
X_test = scaler.transform(X_test)
del df_train
y_train = pd.get_dummies(y_train).values
y_val = pd.get_dummies(y_val).values

X_train = X_train.reshape(-1,28,28,1)
X_val = X_val.reshape(-1,28,28,1)
X_test = X_test.reshape(-1,28,28,1)
# model
model_name = 'best_model'

checkpoint = ModelCheckpoint(results_path+model_name+'.hdf5', monitor='val_acc', 
    verbose=1, save_best_only=True, save_weights_only=False,
    mode='auto', period=1)
earlystopping = EarlyStopping(monitor='val_acc', min_delta=0, patience=3,
    verbose=1, mode='auto')

n_filters = 64
n_dense = 100
model = Sequential()
model.add(Conv2D(filters = n_filters, kernel_size = (3,3),padding = 'Same', 
                 activation ='relu'))
model.add(Conv2D(filters = n_filters, kernel_size = (3,3),padding = 'Same', 
                 activation ='relu'))
model.add(MaxPool2D(pool_size=(2,2)))
model.add(Conv2D(filters = n_filters, kernel_size = (3,3),padding = 'Same', 
                 activation ='relu'))
model.add(Conv2D(filters = n_filters, kernel_size = (3,3),padding = 'Same', 
                 activation ='relu'))
model.add(MaxPool2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dense(y_train.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# fit
history = model.fit(X_train, y_train, epochs=100, batch_size=100,
    verbose=1,shuffle=True,validation_data=(X_val,y_val),
    callbacks=[checkpoint,earlystopping])
model.summary()

# training on val 
# load best model so far 
model.load_weights(results_path+model_name+'.hdf5')
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
history = model.fit(X_val, y_val, epochs=1, batch_size=100,
    verbose=1,shuffle=True)
# treinado ate atingir em torno do train loss anterior
model.save_weights(results_path+model_name+'.hdf5')

'''
# load model
model.load_weights(results_path+'best_model.hdf5')
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

best_model  0.98614 LB
train: acc train 9941  acc val 9877 loss train 0.0254
val train: loss: 0.0198 - acc: 0.9937

model structure
n_filters = 64
n_dense = 100
model = Sequential()
model.add(Conv2D(filters = n_filters, kernel_size = (3,3),padding = 'Same', 
                 activation ='relu'))
model.add(Conv2D(filters = n_filters, kernel_size = (3,3),padding = 'Same', 
                 activation ='relu'))
model.add(MaxPool2D(pool_size=(2,2)))
model.add(Conv2D(filters = n_filters, kernel_size = (3,3),padding = 'Same', 
                 activation ='relu'))
model.add(Conv2D(filters = n_filters, kernel_size = (3,3),padding = 'Same', 
                 activation ='relu'))
model.add(MaxPool2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dense(y_train.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

'''


df_pred = pd.DataFrame([])
predictions = model.predict(X_test)
df_pred['ImageId'] = df_test.index + 1
df_pred['Label'] = np.argmax(predictions,axis=1)
del df_test, predictions
df_pred.to_csv(results_path+'predictions.csv',index=False)
